<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PageContent extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function template()
    {
      return $this->belongsTo('App\Models\Template');
    }
}
