<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    //
    protected $guarded = [];
    public $timestamps = false;
    // public function page()
    // {
    //   return $this->belongsTo('App\Models\Page');
    // }

    public function templateGroup()
    {
      return $this->belongsTo('App\Models\TemplateGroup');
    }
}
