<?php

namespace App\Http\Controllers\Pages\Admin;

use App\Http\Controllers\Controller;
use App\Models\Template;
use App\Models\TemplateGroup;
use App\Models\PageContent;
use App\Models\Page;
use Illuminate\Http\Request;
use App\Jobs\createTemplate;
use Storage, ZipArchive;
use Cache;

class TemplatesController extends Controller
{

    public function list()
    {
      $templates = Template::
        orderBy('page_id','desc')->
        with('page')->
        paginate(36);

      return view('adminpages.templatelist', [
          'Templates' => $templates
      ]);
    }

    /**
    * show and edit static templates in webpage
    * @param templateId  as int
    * @param Illuminate\Http\Request
    * @return \Illuminate\Http\Response
    */
    public function showEditTemplate(int $templateId = 0, Request $request)
    {

      //list of all pages for selecting
      $pages = 'App\Models\Page'::all();
      $template = Template::firstOrNew(['id' => $templateId]);

      if(!$this->hasInput($request)){
          return view('adminpages.templateedit', [
              'Template' => $template,
              'Pages' => $pages
          ]);
      }

      $this->validate($request, [
          'title' => 'nullable|string|max:190',
          'page_id' => 'nullable|integer|min:1',
          'template' => 'required|string|max:8096',
      ]);

      //update input if they are provided
      $inputnames = ['template'];
      if($request->page_id) array_push($inputnames, 'page_id');
      if($request->title) array_push($inputnames, 'title');

      $input = $request->only($inputnames);

      // updating template
      $template->fill($input);
      $template->save();

      //clearing page cache
      $page = 'App\Models\Page'::find($template->page_id);
      Cache::forget(url($page->pageview != 'index'? '/'.$page->pageview : ''));

      return view('adminpages.templateedit', [
          'Template' => $template,
          'Pages' => $pages
      ]);
    }

    public function uploadtemplate(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|mimes:zip|max:30120'
        ]);
        $file = $request->file('file');
        $filename = pathinfo(
                    $file->getClientOriginalName()
                )['filename'];
        $templatename = $request->templatename ??  $filename;
        $path = $file->store('uploads/templates');

        return $this->createTemplate(
            $path,
            $templatename,
            $file->getClientOriginalName()
        );

      // return $file->getClientOriginalName();
    }


    private function createTemplate(string $path, string $templatename, string $filename)
    {
        Storage::deleteDirectory('temp');
        $zip = new ZipArchive;
        $res = $zip->open(storage_path('app/'.$path));
        if ($res === TRUE) {
          $zip->extractTo(storage_path('app/temp'));
          $zip->close();
        } else {
          abort('file upload problem', 500);
        }

        $conflicts = $result = $template_assets = [];
         
        $allfiles = Storage::allfiles('temp');
        foreach ( $allfiles as $key => $value) {
          $fullfile =  substr($value,5); // address of file relatively to template root
          $fileinfo = pathinfo($fullfile);
          if(file_exists(public_path($fullfile)) ){
            array_push($conflicts, $fullfile);
          }
        }

        if(count($conflicts)){
          return response()->json([
            'code'=> 1 ,
            'message' => 'فایلهایی با اسامی فایل های شما در سرور وجود دارد',
            'files'=> $conflicts,
          ],400);
        }
        $templategroup = TemplateGroup::create([
            'template_file'=> $path,
            'created_at'=> time() 
        ]);
        foreach ( $allfiles as $key => $value) {
          $fullfile =  substr($value,5); // address of file relatively to template root
          $fileinfo = pathinfo($fullfile);
          // filename
          if($fileinfo['dirname'] != "." || strtolower($fileinfo['extension']) != "html"){
            Storage::disk('public')->put($fullfile, Storage::get($value));
            array_push($template_assets, $fullfile);
          }else{
            $template = Template::create([
              'template_group_id' => $templategroup->id,
              'title' => $templatename,
              'html_file' => $fileinfo['basename'],
              'description' => $fileinfo['basename']
              ]);
            
            $template->update([
              "view" => "autocompiled_".$template->id
            ]);
            $exres = 
                exec(
                "html2blade ".
                    $template->id .' "'.
                    storage_path("app/$value").'" "'.
                    base_path('resources/views/pages/autocompiled_'.$template->id.'.blade.php').'" "'.
                    storage_path('app/temp/'.$template->id.'.json').'"'
                )
            ;
            $template->update([
              "default_content" => Storage::get('temp/'.$template->id.'.json')
            ]);
            array_push($result,$template->id);
          }
        }
        $templategroup->update(['assets' => json_encode($template_assets,true)]);

        return Template::findMany($result);
    }

    public  function deletetemplate(Request $request){
        Cache::flush();
        $template =  Template::find($request->id);
        abort_if(!$template, 400,'no such template');
        // delete page contents related 
        $pagecontents = PageContent::where('template_id',$template->id)->get();
        $relatedpages = $pagecontents->pluck('page_id');
        $pagecontents->each->delete();
        // delete page if there is no content left
        foreach($relatedpages as $k=>$v){
            // dd($v);
            if(!PageContent::where('page_id',$v)->count()){
                Page::where('id',$v)->delete();
            }
        }

        // delete view
        Storage::disk('pages')->delete("autocompiled_{$template->id}.blade.php");
        // delete template in table
        $groupid = $template->template_group_id;
        $template->delete();
        if(!Template::where('template_group_id', $groupid)->count()){
            $templategroup = TemplateGroup::find($groupid);
            Storage::disk('public')->delete(json_decode($templategroup->assets,true));
            // dd(
            //     json_decode($templategroup->assets,true), 
            // );
            $tempfile = $templategroup->template_file;
            $tmppinf = pathinfo($tempfile);
            Storage::move(
                $tempfile,
                $tmppinf['dirname'].'/'.$tmppinf['filename'].'_deleted.'.$tmppinf['extension']
            );
            $templategroup->delete();
        }
        return response()->json(['success']);
        
        // if templategroup doesn't have any template left{
            //delete templategroup
            // delete assets of template group
            // change templatefile name to deleted
            // }
            // 
        

    }

}
