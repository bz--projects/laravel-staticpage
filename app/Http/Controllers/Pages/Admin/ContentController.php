<?php

namespace App\Http\Controllers\Pages\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PageContent;
use App\Models\Page;
use Cache,Storage;

class ContentController extends Controller
{
    //
    public function edit(Request $request)
    {
      Cache::flush();
      $pageurl = $request->pageurl ?? null;

      $page_id = Page::where('url' ,$request->pageurl)->first()->id;
        $template = PageContent::
            where('page_id',$page_id)->
            orderBy('updated_at','desc')->first();
        $finaltemp = array_merge(json_decode($template->content_json, true),$request->pagecontent);
      PageContent::create([
        'content_json' => json_encode($finaltemp,true),
        'page_id' => $page_id,
        'template_id' => $template->template_id,
        'updated_at' => time()
      ]);
      return $request->all();
    }

    public function imageupload(Request $request){
        $this->validate($request, [
            'file' => 'required|mimes:jpeg,bmp,png,jpg,gif|max:1500',
            'url' => 'required'
        ]);
        $file = $request->file('file');
        $filepathinfo = pathinfo($request->url);
        return Storage::disk('public')->putFileAs($filepathinfo['dirname'],$file,$filepathinfo['basename']);
    }
}
