if (!window.$) {
  window.$ = window.jQuery = require("jquery");
}
// require('bootstrap');

window.axios = require("axios");

window.axios.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";

let token = document.head.querySelector('meta[name="csrf-token"]');

window.axios.defaults.headers.common["X-CSRF-TOKEN"] = token.content;

window.$.ajaxSetup({
  headers: {
    "X-CSRF-TOKEN": token.content,
  },
});
