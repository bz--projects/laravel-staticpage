<?php

Auth::routes();

//administration routes
Route::group(['middleware' => 'auth', 'prefix'=> 'admin'], function(){

  //ajax edit url
  Route::post('contentedit','Pages\Admin\ContentController@edit');
  Route::post('contentedit/image','Pages\Admin\ContentController@imageupload');

  Route::get('/pages', 'Pages\PageController@list');
  Route::post('pages/new', 'Pages\PageController@createPage');
  Route::post('/pages/delete', 'Pages\PageController@deletePage');

  Route::post('templates/upload', 'Pages\Admin\TemplatesController@uploadtemplate');
  Route::post('templates/delete', 'Pages\Admin\TemplatesController@deletetemplate');


});
Route::redirect('/home', '/admin/pages');
Route::redirect('/admin', '/admin/pages');

Route::post('/contactus', 'Pages\PageController@contactus');
Route::get('/{pageurl?}', 'Pages\PageController@show');

// routefor captcha
Route::any('captcha', function() {
    return captcha();
});
